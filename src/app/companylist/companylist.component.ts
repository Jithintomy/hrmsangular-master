import { Component, OnInit } from '@angular/core';
import { ListCompany } from '../class/list-company';
import { DataService } from '../service/data.service';
import { SortEvent } from 'primeng/api';
import { MessageService } from 'primeng/api';

import { ConfirmationService } from 'primeng/api';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-companylist',
  templateUrl: './companylist.component.html',
  styleUrls: ['./companylist.component.scss'],
  providers: [MessageService, ConfirmationService]
})
export class CompanylistComponent implements OnInit {
  list: ListCompany[];
  cols: any[];

  first = 0;

  rows = 10;
  DialogVisible = false;
  
  constructor(public router:Router, private data: DataService, private messageService: MessageService, private confirmation: ConfirmationService) {
  }

  ngOnInit(): void {
    this.getData();
    this.cols = [
      { field: 'companyCode', header: 'companyCode' },
      { field: 'CompanyName', header: 'CompanyName' },
      { field: 'email', header: 'email' },
      { field: 'Edit', header: 'Edit' }
    ];
  };
  getData() {
    this.data.getCompany()
      .toPromise()
      .then(res => <ListCompany[]>res)
      .then((list) => {
        this.list = list
      });
  }
  next() {
    this.first = this.first + this.rows;
  }
  prev() {
    this.first = this.first - this.rows;
  }
  reset() {
    this.first = 0;
    this.getData()

  }
  isLastPage(): boolean {
    return this.first === (this.list.length - this.rows);
  }
  isFirstPage(): boolean {
    return this.first === 0;
  }
  edit(companyId) {
    this.router.navigate(['/CompanySetting'],{queryParams:{CompanyID:companyId}});

    console.log(companyId)
  }
  customSort(event: SortEvent) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;
      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
      return (event.order * result);
    });
  }



  //////////////////
  addCompany() {
    // this.
  }
  deleteCompany(id) {
    console.log('got call');
    this.confirmation.confirm({
      key: 'confirm-delete-company',
      message: 'Are you sure to remove all data?',
      accept: () => {
        this.delete(id);
      },
      reject: () => {
        this.messageService.add({ summary: 'Cancelled', severity: 'error',  sticky: false, detail: "You have cancelled the Delete" });

      }
    });
  }
  delete(id) {
    this.data.deleteCompany(id)
      .subscribe((data:any) => {
        console.log(data);
        this.messageService.add({ summary: 'deleted', severity: 'success',  sticky: false, detail: data.Status });
      })
  }
}
