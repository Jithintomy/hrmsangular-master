import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompanylistComponent } from './companylist.component';

const routes: Routes = [{ path: '', component: CompanylistComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanylistRoutingModule { }
