import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanylistRoutingModule } from './companylist-routing.module';
import { CompanylistComponent } from './companylist.component';
import {ButtonModule} from 'primeng/button';
import { ToastModule } from 'primeng/toast';

import { ConfirmDialogModule} from 'primeng/confirmdialog';
import {TableModule} from 'primeng/table';
@NgModule({
  declarations: [CompanylistComponent],
  imports: [
    CommonModule,
    CompanylistRoutingModule,
    TableModule,
    ButtonModule,
    ToastModule,
    ConfirmDialogModule
  ]
})
export class CompanylistModule { }
