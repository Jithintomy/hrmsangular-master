import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { formatDate } from '@angular/common';
import { DataService } from 'src/app/service/data.service';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.scss']
})

export class BankInfoComponent implements OnInit {

  bankBranchList: any;
  accountNumber: any;
  branchId: any;
  bankArray: any = [];
  // branchList:any;
  constructor(private data: DataService) {

  }
  @Input() regForm: FormGroup;


  ngOnInit(): void {
    this.getList()
  }

  getList() {
    this.data.getBankBranch()
      .pipe(
        map(data => {
          return data;
        })
      )
      .subscribe((datas:any) => {
        this.bankBranchList = datas.bankbranchList;
      },
        (error) => {
          this.bankBranchList = [''];
        });
  }
  addBranch(id, acc) {
    const companyBankList = this.regForm.get("companyBankList") as FormArray;
    let details = {
      companyId: null,
      BankBranchID: id,
      BankAccountNo: acc
    };
    if (id != null || acc != null) {
      companyBankList.push(new FormControl(details));
      this.bankArray.push(details);
    }
  }
  delete(data) {
    const companyBankList = this.regForm.get("companyBankList") as FormArray;
    let i: number = 0;
    companyBankList.controls.forEach((item: FormControl) => {
      if (item.value.BankAccountNo === data) {
        companyBankList.removeAt(i);
        return;
      }
      i++;
    });
    i = 0;
    this.bankArray.forEach((res) => {
      if (res.BankAccountNo == data) {
        this.bankArray.splice(i,1);
      }
    });

  }

  bankSubmit() {

    this.regForm.get('companyBankList').updateValueAndValidity();
    this.regForm.get('finYrStartDate').markAsTouched();
    this.regForm.get('finYrStartDate').updateValueAndValidity();
    this.regForm.get('bookStartDate').markAsTouched();
    this.regForm.get('bookStartDate').updateValueAndValidity();
    this.regForm.get('companyStartDate').markAsTouched();
    this.regForm.get('companyStartDate').updateValueAndValidity();
    alert("Updated Successfully");
  }



}

