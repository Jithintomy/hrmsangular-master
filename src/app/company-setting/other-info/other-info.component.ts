import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { FormGroup } from '@angular/forms';
import { map } from 'rxjs/internal/operators/map';

@Component({
  selector: 'app-other-info',
  templateUrl: './other-info.component.html',
  styleUrls: ['./other-info.component.scss']
})
export class OtherInfoComponent implements OnInit {
  designationList: any;
  constructor(private data: DataService) { }
  @Input() regForm: FormGroup;


  ngOnInit(): void {
    this.getList()
  }

  getList() {
    this.data.getDesignationList()
    .pipe(
      map(data => {
        return data;
      })
    )
      .subscribe((datas:any) => {
        this.designationList = datas.designationList;
      },
        (error) => {
          this.designationList = [''];
        });
  }
  otherSubmit() {
    this.regForm.get('officeHeadName').markAsTouched();
    this.regForm.get('officeHeadName').updateValueAndValidity();
    this.regForm.get('fatherName').markAsTouched();
    this.regForm.get('fatherName').updateValueAndValidity();
    this.regForm.get('designationId').markAsTouched();
    this.regForm.get('designationId').updateValueAndValidity();
    this.regForm.get('panNo').markAsTouched();
    this.regForm.get('panNo').updateValueAndValidity();
    this.regForm.get('tanNo').markAsTouched();
    this.regForm.get('tanNo').updateValueAndValidity();
    this.regForm.get('gstTIN').markAsTouched();
    this.regForm.get('gstTIN').updateValueAndValidity();
    this.regForm.get('cinNo').markAsTouched();
    this.regForm.get('cinNo').updateValueAndValidity();
    this.regForm.get('rocCode').markAsTouched();
    this.regForm.get('rocCode').updateValueAndValidity();
    alert("Updated Successfully");
  }

}
