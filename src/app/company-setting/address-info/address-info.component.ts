import { Component, OnInit,Input} from '@angular/core';
import {FormGroup,FormControl,FormArray} from '@angular/forms';
import {map} from 'rxjs/internal/operators/map'
import { DataService } from 'src/app/service/data.service';
import { isNgTemplate } from '@angular/compiler';


@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html',
  styleUrls: ['./address-info.component.scss']
})
export class AddressInfoComponent implements OnInit {
  provinceList:any;
  dataLoad:boolean=false;
  industryList:any;
  weekData:any=[
    {id:1 ,name:'Monday'},
    {id:2,name :'Tuesday'},
    {id:3,name:'Wednesday'},
    {id:4,name:'Thursday'},
    {id:5,name:'Friday'},
    {id:6,name:'Saturday'},
    {id:7,name:'Sunday'}

  ]
  constructor(private data:DataService) { }

  @Input() regForm:FormGroup;

  ngOnInit(): void {
    this.getList();
  }
  getList() {
    this.data.getProvince()
      .subscribe((datas: any) => {
        this.provinceList = datas.provinceList ? datas.provinceList : []
      },
        (error) => {
          console.log(error)
        });
  }

  onCheckboxChange(e) {
    const checkArray: FormArray = this.regForm.get('daysID') as FormArray;
    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }
addressSubmit(){
  this.regForm.get('area').markAsTouched();
  this.regForm.get('area').updateValueAndValidity();
  this.regForm.get('block').markAsTouched();
  this.regForm.get('block').updateValueAndValidity();
  this.regForm.get('street').markAsTouched();
  this.regForm.get('street').updateValueAndValidity();
  this.regForm.get('city').markAsTouched();
  this.regForm.get('city').updateValueAndValidity();
  this.regForm.get('province').markAsTouched();
  this.regForm.get('province').updateValueAndValidity();
  this.regForm.get('primaryEmail').markAsTouched();
  this.regForm.get('primaryEmail').updateValueAndValidity();
  this.regForm.get('secondaryEmail').markAsTouched();
  this.regForm.get('secondaryEmail').updateValueAndValidity();
  this.regForm.get('industry').markAsTouched();
  this.regForm.get('industry').updateValueAndValidity();
  this.regForm.get('companyType').markAsTouched();
  this.regForm.get('companyType').updateValueAndValidity();
  this.regForm.get('website').markAsTouched();
  this.regForm.get('website').updateValueAndValidity();

  this.regForm.get('otherInformation').markAsTouched();
  this.regForm.get('otherInformation').updateValueAndValidity();
  this.regForm.get('dayID').markAsTouched();
  alert("Update Successfully");
  
}
}
