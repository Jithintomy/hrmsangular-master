import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';

@Component({
  selector: 'app-logo-info',
  templateUrl: './logo-info.component.html',
  styleUrls: ['./logo-info.component.scss']
})
export class LogoInfoComponent implements OnInit {

  files: any[] = [];
  form: FormGroup;
  imageUrl: any;
  fileName: string = '';
  file: File;
  constructor(private data: DataService) { }
  @Input() regForm: FormGroup;
  ngOnInit(): void {
  }
  imgUpload(event) {
    const file = (event.target as HTMLInputElement).files[0];
    
    const reader = new FileReader();
    reader.onload = () => {
      this.imageUrl = reader.result as string;
      // console.log(this.imageUrl)
      this.regForm.patchValue({
        companyLogo: this.imageUrl
      });
      this.regForm.get('companyLogo').updateValueAndValidity();
    }
    reader.readAsDataURL(file)
    

  }

  submit() {
    this.regForm.get('companyLogo').markAsTouched();
    this.data.createCompany(this.regForm.value);
  }
}
