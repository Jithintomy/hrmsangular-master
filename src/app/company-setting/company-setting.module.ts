import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompanyInfoComponent } from './company-info/company-info.component';
import { AddressInfoComponent } from './address-info/address-info.component';
import { BankInfoComponent } from './bank-info/bank-info.component';
import { OtherInfoComponent } from './other-info/other-info.component';
import { LogoInfoComponent } from './logo-info/logo-info.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


import { AngularMaterialModule } from './../Module/material/material.module';
import { ProgressComponent } from '../components/progress/progress.component';
import { ToastModule } from 'primeng/toast';

import { DndDirective } from './../directive/dragAndDrop.directive';
import {CompanySettingComponent} from './../company-setting/company-setting.component'
import { CompanySettingRoutingModule } from './company-setting-routing.module';
@NgModule({
    declarations:[DndDirective,CompanySettingComponent,CompanyInfoComponent,AddressInfoComponent,BankInfoComponent,OtherInfoComponent,LogoInfoComponent,ProgressComponent],
    imports:[
        CommonModule,
        CompanySettingRoutingModule,
        AngularMaterialModule,
        ToastModule,
        ReactiveFormsModule,FormsModule,

    ]
})
export class CompanySettingModule{}