import{FormGroup,FormControl,FormBuilder,Validators,FormArray}  from '@angular/forms';
  import { from } from 'rxjs';
  import {DataService} from '../service/data.service'
  import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

     
@Component({
  selector: 'app-company-setting',
  templateUrl: './company-setting.component.html',
  styleUrls: ['./company-setting.component.scss']
})
export class CompanySettingComponent implements OnInit {
  currentPage:'General settings';
  defaultPage:'company profile';
  companyInfoForm:FormGroup;
  public sub:any;
  CompanyIDs: number;
  companybankInfo: any;
  constructor( public route:ActivatedRoute,private data:DataService, fb:FormBuilder) {
    this.data.currentPage=this.currentPage;
    this.data.defaultPage=this.defaultPage
   }

  
   ngOnInit(): void {
    this.sub=this.route
    .queryParams
    .subscribe(params=>{
      this.CompanyIDs= +params['CompanyID'] || 0;
      if(this.CompanyIDs>0)
      this.FindCompanyByID(this.CompanyIDs);
    });
    const web = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.companyInfoForm = new FormGroup({
      'selectorMode': new FormControl('', [Validators.required]),
   'CompanyId': new FormControl('', [Validators.required]),
      'companyName': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]),
      'branchName': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]),
      'employerCode': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]),
      'shortName': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
      'country': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
      'currency': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
      'companyType': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),

      'area': new FormControl('', [Validators.required]),
      'block': new FormControl('', [Validators.required]),
      'street': new FormControl('', [Validators.required]),
      'city': new FormControl('', [Validators.required]),
      'province': new FormControl('', [Validators.required]),
      'primaryEmail': new FormControl('', [Validators.required, Validators.email]),
      'secondaryEmail': new FormControl('', [Validators.required, Validators.email]),
      'industry': new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(50)]),
      'OtherInformation': new FormControl('', [Validators.required]),
      'website': new FormControl('', [Validators.required, Validators.pattern(web)]),
      "daysID": new FormArray([], [Validators.required]),

      "finYrStartDate": new FormControl('', [Validators.required]),
      "bookStartDate": new FormControl('', [Validators.required]),
      "companyStartDate": new FormControl('', [Validators.required]),

      "companyBankList": new FormArray([], Validators.required),

      "officeHeadName": new FormControl('', [Validators.required]),
      "fatherName": new FormControl('', [Validators.required]),
      "designationId": new FormControl('', [Validators.required]),
      "panNo": new FormControl('', [Validators.required]),
      "tanNo": new FormControl('', [Validators.required]),
      "cinNo": new FormControl('', [Validators.required]),
      "gstTIN": new FormControl('', [Validators.required]),
      "rocCode": new FormControl('', [Validators.required]),
      "registrationNo": new FormControl('', [Validators.required]),

      "companyLogo": new FormControl(null),
    })
  }
  public FindCompanyByID(id){
    this.data.getCompanyByID(id)
    .subscribe((data:any)=>{
      console.log(data)
      this.companyInfoForm.get('companyName').setValue( data.companyInfo.companyName)
      this.companyInfoForm.get('branchName').setValue( data.companyInfo.branchName)
      this.companyInfoForm.get('employerCode').setValue( data.companyInfo.EPID)
      this.companyInfoForm.get('shortName').setValue( data.companyInfo.shortName)
      this.companyInfoForm.get('country').setValue( data.companyInfo.countryId)
      this.companyInfoForm.get('currency').setValue(data.companyInfo.currencyId)
      this.companyInfoForm.get('industry').setValue(data.companyInfo.companyIndustryId)

      this.companyInfoForm.get('area').setValue(data.companyInfo.area)
      this.companyInfoForm.get('block').setValue(data.companyInfo.block)
      this.companyInfoForm.get('city').setValue(data.companyInfo.city)
      this.companyInfoForm.get('street').setValue(data.companyInfo.road)
      this.companyInfoForm.get('province').setValue(data.companyInfo.stateId)
      
      this.companyInfoForm.get('primaryEmail').setValue(data.companyInfo. primaryEmail)
      this.companyInfoForm.get('secondaryEmail').setValue(data.companyInfo.secondaryEmail)
      this.companyInfoForm.get('OtherInformation').setValue(data.companyInfo. OtherInfo)
      this.companyInfoForm.get('website').setValue(data.companyInfo.webSite)
      this.companyInfoForm.get('daysID').setValue(data.companyInfo.daysID)


      this.companyInfoForm.get('finYrStartDate').setValue(data.companyInfo.finYrStartDate)
      this.companyInfoForm.get('bookStartDate').setValue(data.companyInfo.bookStartDate)
      this.companyInfoForm.get('companyStartDate').setValue(data.companyInfo.companyStartDate)
      this.companyInfoForm.get('companyBankList').setValue(data.companyInfo.companyBankList)



      this.companyInfoForm.get('officeHeadName').setValue(data.companyInfo.officeHeadName)
      this.companyInfoForm.get('fatherName').setValue(data.companyInfo.fatherName)
      this.companyInfoForm.get('designationId').setValue(data.companyInfo.designationId)
      this.companyInfoForm.get('panNo').setValue(data.companyInfo.panNo)
      this.companyInfoForm.get('cinNo').setValue(data.companyInfo.cinNo)
      this.companyInfoForm.get('gstTIN').setValue(data.companyInfo.gstTIN)
      this.companyInfoForm.get('rocCode').setValue(data.companyInfo.rocCode)
      this.companyInfoForm.get('registrationNo').setValue(data.companyInfo.registrationNo)




    })
  }
}