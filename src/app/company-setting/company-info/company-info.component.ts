import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { map } from 'rxjs/internal/operators/map';
import { DataService } from 'src/app/service/data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-company-info',
  templateUrl: './company-info.component.html',
  styleUrls: ['./company-info.component.scss']
})
export class CompanyInfoComponent implements OnInit {
  companyList: any;
  selectorMode: Boolean = true;
  countryList: any;
  currencyList: any;
  industryList: any;
  typeList: any;
  companyInfo: FormGroup;


  constructor(private data: DataService, private _snackBar: MatSnackBar) {
    this.getList();
  }
  @Input() regForm: FormGroup;

  ngOnInit(): void { }
  getList() {
    this.data.getCompany()
      .subscribe((datas) => {
        this.companyList = datas ? datas : []
      },
        (error) => {
          console.log
        
        });
    this.data.getCountry() 
      .pipe(
        map(data => {
          return data;
        })
      )
      .subscribe((datas: any) => {
        this.countryList = datas.countryList ? datas.countryList : []
      },
        (error) => {
          console.log(error)
          
        });
    this.data.getCurrency()
      .subscribe((datas: any) => {
        this.currencyList = datas.currencyList ? datas.currencyList : [],
          (error) => {
            console.log(error)
          }
      });
    this.data.getIndustry()
      .subscribe((datas: any) => {
        this.industryList = datas.industryList ? datas.industryList : [],
          (error) => {
            console.log(error)
          }
      });
    this.data.getTypeList()
      .subscribe((datas: any) => {
        // console.log(datas)
        this.typeList = datas.typeList ? datas.typeList : [],
          (error) => {
            console.log(error)
          }
      });
  }
  onCompanyNameSelect(e) {
    const CompanyName: FormControl = this.regForm.get('companyName') as FormControl;
    CompanyName.setValue(e.target.textContent.trim());

  }


  // if (this.regForm.get('selectorMode').value === "false")

  get companyIdFn(): any { return this.regForm.get('companyId') }


  submit() {
    this.regForm.get('selectorMode').markAsTouched();
    this.regForm.get('selectorMode').updateValueAndValidity();
    this.regForm.get('companyName').markAsTouched();
    this.regForm.get('companyName').updateValueAndValidity();
    if (this.regForm.get('selectorMode').value === 'false') {
      this.regForm.get('CompanyId').markAllAsTouched();
      this.regForm.get('CompanyId').updateValueAndValidity()
    }
    else {
      this.regForm.get('CompanyId').setValue('');
      this.regForm.get('CompanyId').updateValueAndValidity();
    }
    this.regForm.get('branchName').markAsTouched();
    this.regForm.get('branchName').updateValueAndValidity();
    this.regForm.get('employerCode').markAsTouched();
    this.regForm.get('employerCode').updateValueAndValidity();
    this.regForm.get('shortName').markAsTouched();
    this.regForm.get('shortName').updateValueAndValidity();
    this.regForm.get('country').markAsTouched();
    this.regForm.get('country').updateValueAndValidity();
    this.regForm.get('currency').markAsTouched();
    this.regForm.get('currency').updateValueAndValidity();
    this.regForm.get('industry').markAsTouched();
    this.regForm.get('industry').updateValueAndValidity();
    this.regForm.get('companyType').markAsTouched();
    this.regForm.get('companyType').updateValueAndValidity();
    console.log(this.regForm)
    this._snackBar.open('Updated Successfully!!', 'End now', {
      duration: 1000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
    });
  }
}
