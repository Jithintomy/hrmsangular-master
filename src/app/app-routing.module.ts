import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:'',redirectTo:'companylist',pathMatch:'full'},
  { path: 'CompanySetting', loadChildren: () => import('./company-setting/company-setting.module').then(m => m.CompanySettingModule) }, { path: 'recuritment',loadChildren:()=> import('./recuritment/recuritment.module').then(m=>m.RecuritmentModule)}, { path: 'companylist', loadChildren: () => import('./companylist/companylist.module').then(m => m.CompanylistModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
