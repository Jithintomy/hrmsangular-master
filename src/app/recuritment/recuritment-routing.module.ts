import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RecuritmentComponent } from './recuritment.component';

const routes: Routes = [{ path: '', component: RecuritmentComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecuritmentRoutingModule { }
