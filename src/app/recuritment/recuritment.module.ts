import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecuritmentRoutingModule } from './recuritment-routing.module';
import { RecuritmentComponent } from './recuritment.component';


@NgModule({
  declarations: [RecuritmentComponent],
  imports: [
    CommonModule,
    RecuritmentRoutingModule
  ]
})
export class RecuritmentModule { }
