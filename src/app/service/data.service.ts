import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { formatDate } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  currentPage: string;
  defaultPage: string;
  APIList: any = {};
  formData: FormGroup;
  companyName: string;
  baseurl = 'http://sajumindsoft-001-site1.btempurl.com/';
  constructor(private http: HttpClient) {
    let masterList = this.baseurl + 'api/masterListAPI/';
    let companyAPI = this.baseurl + 'api/CompanyAPI/'
    this.APIList = {
      companyList: companyAPI + 'CompanyList',
      countryList: masterList + 'CountryList',
      industryList: masterList + 'IndustryList',
      typeList: masterList + 'TypeList',
      currencyList: masterList + 'CurrencyList',
      provinceList: masterList + 'ProvinceList',
      bankBranchList: masterList + 'BankBranchList',
      BankList: masterList + 'BankList',
      designationList: masterList + 'DesignationList',
      createCompany: companyAPI + 'createCompany',
      deleteCompany: companyAPI + 'DeleteCompany',
      getCompanyByID: companyAPI + 'FindCompanyById'
    }
  }
  getCompany() {
    return this.http.get(this.APIList.companyList)
  }
  getCountry() {
    return this.http.get(this.APIList.countryList)
  }
  getIndustry() {
    return this.http.get(this.APIList.industryList)
  };
  getCurrency() {
    return this.http.get(this.APIList.currencyList)
  }
  getTypeList() {
    return this.http.get(this.APIList.typeList)
  }
  getProvince() {
    return this.http.get(this.APIList.provinceList)
  }
  getBankBranch() {
    return this.http.get(this.APIList.bankBranchList)
  }
  getBank() {
    return this.http.get(this.APIList.BankList);
  }
  getDesignationList() {
    return this.http.get(this.APIList.designationList)
  }

  deleteCompany(id) {
    return this.http.get(this.APIList.deleteCompany + '?companyId=' + id)
  }
  getCompanyByID(id) {
    return this.http.get(this.APIList.getCompanyByID + '?CompanyID=' + id)
  }




  dateFormat(date) {
    if (date)
      return formatDate(new Date(date), 'dd-MM-yyyy hh:mm:ssZZZZZ', 'en_US')
  }
  getCompanyByName(id) {
    this.getCompanyByID(id)
      .subscribe((data: any) => {
        this.companyName = data.companyInfo.companyName
        return data.companyInfo.companyName.block
      });

    return this.companyName;
  }

  createCompany(regFormData) {
    if (regFormData.selectorMode && regFormData.companyName && regFormData.shortName
      && regFormData.area && regFormData.block && regFormData.city && regFormData.province && regFormData.country
      && regFormData.currency && regFormData.industry && regFormData.primaryEmail && regFormData.secondaryEmail && regFormData.website
      && regFormData.finYrStartDate && regFormData.bookStartDate && regFormData.daysID.length > 0
      && regFormData.bookStartDate && regFormData.employerCode && regFormData.companyStartDate && regFormData.OtherInformation
      && regFormData.companyLogo && regFormData.tanNo && regFormData.officeHeadName && regFormData.designationId && regFormData.panNo
      && regFormData.panNo && regFormData.fatherName && regFormData.cinNo && regFormData.gstTIN && regFormData.rocCode
      && regFormData.registrationNo && (regFormData.companyBankList.length > 0 || regFormData.companyBankList)) {
      let bodyValue = {

        companyInfo: {
          isBranch: regFormData.selectorMode === "true" ? 0 : 1,
          parentId: regFormData.selectorMode === "false" ? regFormData.CompanyId : 0,
          companyId: regFormData.selectorMode === "false" ? regFormData.CompanyId : null,
          companyName: regFormData.companyName,
          branchName: regFormData.selectorMode === "false" ? regFormData.branchName : null,
          shortName: regFormData.shortName,
          pOBox: null,
          area: regFormData.area,
          block: regFormData.block,
          road: regFormData.street,
          city: regFormData.city,
          stateId: regFormData.province,
          countryId: regFormData.country,
          currencyId: regFormData.currency,
          companyIndustryId: regFormData.industry,
          companyTypeId: null,
          primaryEmail: regFormData.primaryEmail,
          secondaryEmail: regFormData.secondaryEmail,
          telephoneNo: null,
          faxNo: null,
          webSite: regFormData.website,
          finYrStartDate: this.dateFormat(regFormData.finYrStartDate),
          daysID: regFormData.daysID,
          bookStartDate: this.dateFormat(regFormData.bookStartDate),
          EPID: regFormData.employerCode,
          companyStartDate: this.dateFormat(regFormData.companyStartDate),
          OtherInfo: regFormData.OtherInformation,
          companyLogo: regFormData.companyLogo,
          tanNo: regFormData.tanNo,
          officeHeadName: regFormData.officeHeadName,
          designationId: regFormData.designationId,
          panNo: regFormData.panNo,
          fatherName: regFormData.fatherName,
          cinNo: regFormData.cinNo,
          gstTIN: regFormData.gstTIN,
          rocCode: regFormData.rocCode,
          registrationNo: regFormData.registrationNo
        },
        companybankInfo: regFormData.companyBankList,
        companyBankList: regFormData.companyBankList
      };
      this.http.post(this.APIList.createCompany, bodyValue)
        .subscribe((data: any) => {
          alert(data.Status);
        })
    }
    else
      alert("Oops You have miss something to enter,Please fill all the fields");
  }

}



