import { Component, OnInit } from '@angular/core';
import{DataService} from 'src/app/service/data.service'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentPage='';
  defaultPage='';

  constructor( private data:DataService) { 
    this.currentPage=this.data.currentPage;
    this.defaultPage=this.data.defaultPage;
  }

  ngOnInit(): void {
  }

}
