


import { Component } from '@angular/core';
import {Router,NavigationEnd} from '@angular/router';

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'HRMSAngular';
}
